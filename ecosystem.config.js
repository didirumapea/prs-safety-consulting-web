module.exports = {
  apps : [
    {
      name: "prs-safety-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "prs-safety-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "prs-safety-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
