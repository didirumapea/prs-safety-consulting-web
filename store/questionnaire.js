export const state = () => ({
    questionnaireList: {},

});

export const actions =  {
  async CHECK_USER_ASSESSMENT({commit}, payload) {
    try {
      // const response = await this.$axios.$get(`web/questionnaire/list/trx-uid=${payload}/page=1/limit=100/column-sort=id/sort=desc`)
      // commit('SET_LIST_QUESTIONNAIRE', response.data)
      // console.log(response.data)
    } catch(error) {
      throw error
    }
  },
  async GET_LIST_QUESTIONNAIRE({commit}, payload) {
    try {
      const response = await this.$axios.$get(`web/questionnaire/list/trx-uid=${payload}/page=1/limit=100/column-sort=id/sort=desc`)
      commit('SET_LIST_QUESTIONNAIRE', response.data)
      // console.log(response.data)
    } catch(error) {
      throw error
    }
  },

  async SET_ASSESSMENT({commit}, payload) {
    try {
      const response = await this.$axios.$post(`web/assesment/add`, payload)
      // commit('SET_LIST_QUESTIONNAIRE', response.data)
      // console.log(response.data)
    } catch(error) {
      throw error
    }
  },
  async GET_FACILITIES({commit}) {
    try {
      const response = await this.$axios.$get('api/facilities.json')
      commit('SET_FACILITIES', response.data)
      //console.log(response.data)
    } catch(error) {
      throw error
    }
  },

  async GET_BENEFIT({commit}) {
    try {
      const response = await this.$axios.$get('api/benefit.json')
      commit('SET_BENEFIT', response.data)
      //console.log(response.data)
    } catch(error) {
      throw error
    }
  },

  async GET_EXPERIENCE({commit}) {
    try {
      const response = await this.$axios.$get('api/experience.json')
      commit('SET_EXPERIENCE', response.data)
      //console.log(response.data)
    } catch(error) {
      throw error
    }
  },

};

export const mutations = {
  SET_LIST_QUESTIONNAIRE(state, data) {
    state.questionnaireList = data
  },
  SET_FACILITIES(state, data) {
    state.facilities = data
  },
  SET_BENEFIT(state, data) {
    state.benefit = data
  },
  SET_EXPERIENCE(state, data) {
    state.experience = data
  },
};

export const getters =  {
  facilities(state) {
    return state.facilities;
  },
  benefit(state) {
    return state.benefit;
  },
  experience(state) {
    return state.experience;
  },
};
