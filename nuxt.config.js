import pkg from './package'

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.8.2/css/all.css'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap'},
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.css'},
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'},
    ],
    script: [
      {
        src: "../assets/js/jquery-3.5.1.min.js",
        type: "text/javascript"
      },
      {
        src: "../assets/js/common_scripts.min.js",
        type: "text/javascript"
      },
      {
        src: "../assets/js/velocity.min.js",
        type: "text/javascript"
      },
      {
        src: "../assets/js/common_functions.js",
        type: "text/javascript"
      },
      {
        src: "../assets/js/wizard_without_branch.js",
        type: "text/javascript"
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#008080' },

  /*
  ** Global CSS
  */

  css: [
    '@/static/assets/css/menu.css',
    '@/static/assets/css/style.css',
    '@/static/assets/css/vendors.css',
    '@/static/assets/css/custom.css',
  ],

  router: {
    middleware: [
      'clearValidationErrors'
    ]
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/mixins/validation',
    '~/plugins/global',
    '~/plugins/axios',
    '~/plugins/mixins/user',
    '~/plugins/vue-scrollto',
    { src: '~plugins/ant.js'},
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'vue-sweetalert2/nuxt',
  ],

  server: {
    port: process.env.WEB_PORT, // default: 3000
    host: process.env.NODE_ENV !== "production" ? '0.0.0.0' : '151.106.112.12', // default: localhost,
    // host: '192.168.1.188', // default: localhost,
    // timing: false,
    // https: {
    // 	key: fs.readFileSync(path.resolve(__dirname, '.https/server.key')),
    // 	cert: fs.readFileSync(path.resolve(__dirname, '.https/server.crt'))
    // }
  },
  axios: {
    // ---------- maunual setting ---------
    // proxyHeaders: false,
    // credentials: false,
    baseURL: process.env.NODE_ENV !== "production"
      ? `http://localhost:5432/api/v1/`
      // ? `http://192.168.43.116:5432/api/v1/`
      // ? "http://151.106.112.10:5432/api/v1/"
      : "http://151.106.112.10:5432/api/v1/",
    // ---------- proxy setting -----------
    // proxy: true, // Can be also an object with default options
    // prefix: '/api/'
  },


  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }

  }
}
